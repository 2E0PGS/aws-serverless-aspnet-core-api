﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AwsServerlessAspNetCore.Api.Models;
using System.Globalization;

namespace AwsServerlessAspNetCore.Api.Controllers
{
    [Route("v1/values")]
    public class ValuesController : ControllerBase
    {
        ILogger Logger { get; set; }

        public ValuesController(ILogger<ValuesController> logger)
        {
            this.Logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("I gotcha!");
        }

        [HttpPost]
        public IActionResult Post([FromBody]RequestModel requestModel)
        {
            var responseModel = new ResponseModel();
            responseModel.Motd = "Hello from AWS serverless.";
            responseModel.Data = requestModel.Data;
            responseModel.EnvironmentMachineName = Environment.MachineName;
            responseModel.EnvironmentDirectory = Environment.CurrentDirectory;
            responseModel.EnvironmentSystemDirectory = Environment.SystemDirectory;
            responseModel.CurrentCulture = CultureInfo.CurrentCulture.DisplayName;
            responseModel.CurrentDateTimeUtc = DateTime.UtcNow;
            responseModel.Method = Request.Method;
            return Ok(responseModel);
        }

        [HttpPut]
        public void Put()
        {
        }

        [HttpDelete]
        public void Delete()
        {
        }
    }
}
