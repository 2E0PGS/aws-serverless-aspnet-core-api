using System;

namespace AwsServerlessAspNetCore.Api.Models
{
    public class ResponseModel 
    {
        public string Motd { get;set; }
        public string Data { get;set; }
        public string EnvironmentMachineName { get;set; }
        public string EnvironmentDirectory { get;set; }
        public string EnvironmentSystemDirectory { get;set; }
        public string CurrentCulture { get;set; }
        public DateTime CurrentDateTimeUtc { get;set; }
        public string Method { get;set; }
    }
}